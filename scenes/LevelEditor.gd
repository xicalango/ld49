extends TileMap

signal finished_loading

export (NodePath) var _construction 

var construction_node: Node2D

var standalone = false

#onready var _construction: Node2D = $Construction

var tile_mapping = {
	"stone_triangle": preload("res://scenes/WoodTriangle1.tscn"),
	"wood_triangle": preload("res://scenes/WoodTriangle1.tscn"),
	"wood_plank_1": preload("res://scenes/WoodPlank1.tscn"),
	"wood_plank_3": preload("res://scenes/WoodPlank3.tscn"),
	"wood_square": preload("res://scenes/WoodPlank2.tscn"),
	"stone_plank_1": preload("res://scenes/StonePlank1.tscn"),
	"ballwithface": preload("res://scenes/BallWithFace.tscn")
}

func _ready():
	if _construction.is_empty():
		construction_node = Node2D.new()
		add_child(construction_node)
		standalone = true
	else:
		construction_node = get_node(_construction)

	var used_cells = get_used_cells()
	
	for cell in used_cells:
		var cell_id = get_cell(cell.x, cell.y)
		var cell_name = tile_set.tile_get_name(cell_id)
		var map_pos = map_to_world(cell)
		
		if cell_name.length() == 0:
			continue
		
		var scn: Node2D = tile_mapping[cell_name].instance()
		var sprite: Sprite = scn.get_node("sprite")
		
		var offset: Vector2 = Vector2()
		
		var rotation = getRotation(cell.x, cell.y)[0]
		
		if cell_name == "ballwithface":
			offset = Vector2(40,40)
		else:
			if rotation == 0:
				if sprite.region_enabled:
					offset = sprite.region_rect.size / 2
				else:
					offset = sprite.texture.get_size() / 2
			else:
				if sprite.region_enabled:
					var rotated = sprite.region_rect.size.rotated(PI/2)
					offset = rotated / 2
					offset = offset - Vector2(rotated.x, 0)
				else:
					var rotated = sprite.texture.get_size().rotated(PI/2)
					offset = rotated / 2
					offset = offset - Vector2(rotated.x, 0)

		scn.rotation = rotation
		scn.position = map_pos + offset
		
		if standalone:
			scn.mode = RigidBody2D.MODE_STATIC
		
		construction_node.add_child(scn)
	
	if not _construction.is_empty():
		queue_free()
		
	emit_signal("finished_loading")

func _input(event):
	if event.is_action_pressed("ui_accept"):
		construction_node.visible = not construction_node.visible

func getRotation(x:int, y:int) -> Array:
	var transpose = is_cell_transposed(x, y)
	var xflip = is_cell_x_flipped(x, y)
	var yflip = is_cell_y_flipped(x, y)
	if (!xflip and !transpose):
		return [0.0, yflip]
	elif(xflip and !transpose):
		return [PI, !yflip]
	elif(!xflip and transpose):
		return [-PI/2, !yflip]
	elif(xflip and transpose):
		return [PI/2, yflip]
	assert(false)
	return [0,0]

