extends Camera2D

onready var _cannon: Node2D = $"../Cannon"
onready var _balls: Node2D = $"../Balls"

export(float) var grow_radius = 0
export var max_zoom = 2.5
export var min_zoom = 1

export(int) var max_x = 2560
export(int) var min_y = -800

export(float) var zoom_speed = .1

export(int) var scroll_margin = 100

export(float) var scroll_speed = .2

var zoom_target = Vector2(1, 1)

var state_change = false

var zoom_out = false

func do_zoom_out():
	if not zoom_out:
		zoom_out = true
		state_change = true
		drag_enabled = false
		
func do_zoom_in():
	if zoom_out:
		zoom_out = false
		state_change = true
		drag_enabled = true

var follow_balls = false

var need_to_reset = false

var force_reset = false

var drag = false

var drag_enabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if state_change:
		state_change = false
		if zoom_out:
			$Tween.interpolate_property(self,
				"zoom",
				zoom,
				Vector2(max_zoom, max_zoom),
				zoom_speed,
				Tween.TRANS_SINE,
				Tween.EASE_OUT
			)
			$Tween.interpolate_property(self,
				"position",
				position,
				Vector2(0, min_y),
				zoom_speed,
				Tween.TRANS_SINE,
				Tween.EASE_OUT
			)
		else:
			$Tween.interpolate_property(self,
				"position",
				position,
				Vector2(0, 0),
				zoom_speed,
				Tween.TRANS_SINE,
				Tween.EASE_OUT
			)
			$Tween.interpolate_property(self,
				"zoom",
				zoom,
				zoom_target,
				zoom_speed,
				Tween.TRANS_SINE,
				Tween.EASE_OUT
			)
		$Tween.start()

func _input(event):
	if drag_enabled:
		if event is InputEventMouseButton:
			drag = event.pressed and event.button_index == BUTTON_RIGHT
		if event is InputEventMouseMotion and drag:
			var dest = position + (event.relative * 50)
			dest.x = clamp(dest.x, 0, max_x - get_viewport_rect().size.x)
			dest.y = clamp(dest.y, min_y, 0)
			$Tween.interpolate_property(self,
				"position",
				position,
				dest,
				scroll_speed,
				Tween.TRANS_SINE,
				Tween.EASE_OUT
			)
			$Tween.start()

func _on_Cannon_started():
	$AnimationPlayer.play("Screenshake")
	do_zoom_out()

func _on_Cannon_stopped():
	$AnimationPlayer.stop()
