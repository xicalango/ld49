extends Node2D

signal started
signal stopped

var started = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var balls_node = $"../Balls"

export (PackedScene) var ball_scene

export var nballs = 100

var power: Vector2 = Vector2()

var current_balls: int = 0

func start():
	current_balls = nballs
	started = true
	emit_signal("started")

func _on_Timer_timeout():
	if current_balls == 0:
		if started:
			started = false
			emit_signal("stopped")
		return
	current_balls -= 1
	var new_ball: RigidBody2D = ball_scene.instance()
	
	new_ball.transform = self.transform
	new_ball.apply_impulse(Vector2(), power)
	
	balls_node.add_child(new_ball)
	
	$Label.text = str(current_balls)
	
