extends Node2D

onready var variables = get_node("/root/Variables")

func _ready():
	randomize()
	variables.num_balls = 10
	$Cannon.disable()

func _on_StartGameButton_pressed():
	variables.current_level = 0
	get_tree().change_scene("res://scenes/Battlefield.tscn")
	

func _on_EnterLevelPassword_pressed():
	var lvl = variables.passwords.find($LineEdit.text)
	if lvl >= 0:
		variables.current_level = lvl
		get_tree().change_scene("res://scenes/Battlefield.tscn")


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_Timer_timeout():
	$Timer.stop()
	$Cannon.reset()
	$Cannon.set_power(randf())
	for ball in $Balls.get_children():
		ball.queue_free()
	$Cannon.start()

func _on_Cannon_stopped():
	$Timer.wait_time = 5
	$Timer.start()


func _on_LineEdit_text_entered(new_text):
	_on_EnterLevelPassword_pressed()
