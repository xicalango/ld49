extends Node2D

onready var variables = get_node("/root/Variables")

var current_level_instance: Node

var state: String = "load_level"
var soft_reset = false

var eval_count: int = 6

func _ready():
	_load_level(variables.current_level)

func _load_level(level_id):
	print("loading level ", level_id)
	state = "load_level"
	if current_level_instance != null:
		remove_child(current_level_instance)
	
	for ball in $Balls.get_children():
		ball.queue_free()
		
	$Cannon.visible = false
	$Numbers.visible = false
	$EvalTimer.stop()
	eval_count = 6
	
	var level = variables.levels[level_id]
	current_level_instance = level.instance()
	current_level_instance.transform = $ConstructionPosition.transform
	current_level_instance.get_node("LevelEditor").connect("finished_loading", self, "_finished_loading")
	variables.num_balls = current_level_instance.num_balls
	add_child(current_level_instance)


func _finished_loading():
	$Cannon.reset()
	if soft_reset:
		_on_NewLevelDialog_confirmed()
	else:
		$"Next Level/NewLevelDialog".dialog_text = "Level: " + current_level_instance.level_name + "\nNumber of balls: " + str(current_level_instance.num_balls) + "\nPassword: " + current_level_instance.password
		$"Next Level/NewLevelDialog".window_title =  current_level_instance.level_name	
		$"Next Level/NewLevelDialog".popup_centered()
	
func _on_Cannon_started():
	for ball in $Balls.get_children():
		ball.queue_free()
	state = "evaluating"

func _on_Cannon_stopped():
	state = "evaluating_stopped"

func _process(delta):
	if state.begins_with("evaluating"):
		if current_level_instance != null:
			var has_enemies = false
			for child in current_level_instance.get_node("Construction").get_children():
				if child.is_in_group("enemy"):
					has_enemies = true
					break
			
			if not has_enemies:
				state = "dialog"
				$Camera2D.do_zoom_in()
				$"Next Level/AcceptDialog".popup_centered()
		
	if state == "evaluating_stopped" and not has_balls():
		state = "evaluating_timer"
		eval_count = 6
		$EvalTimer.start(1)

func has_balls():
	for child in $Balls.get_children():
		if child.is_in_group("ball") == false:
			continue
		return true
	return false

func _input(event):
	var force_reset_request = false
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_LEFT:
			force_reset_request = true
	if event.is_action_pressed("ui_accept"):
		force_reset_request = true
	
	if force_reset_request:
		$Camera2D.do_zoom_in()
		if state == "evaluating_timer":
			$EvalTimer.stop()
			eval_count = 0
			_on_EvalTimer_timeout()
			
	if state == "positioning":
		if event is InputEventMouseButton:
			if event.pressed and event.button_index == BUTTON_WHEEL_UP:
				$Camera2D.do_zoom_in()
			elif event.pressed and event.button_index == BUTTON_WHEEL_DOWN:
				$Camera2D.do_zoom_out()

func _on_AcceptDialog_confirmed():
	variables.current_level += 1
	if variables.current_level >= len(variables.levels):
		get_tree().change_scene("res://scenes/EndOfGame.tscn")
		return
	soft_reset = false
	_load_level(variables.current_level)

func _on_Button_pressed():
	soft_reset = true
	_load_level(variables.current_level)

func _on_NewLevelDialog_confirmed():
	state = "positioning"
	$Cannon.visible = true

func _on_NopeDialog_confirmed():
	soft_reset = true
	_load_level(variables.current_level)

func _on_EvalTimer_timeout():
	if eval_count > 0:
		eval_count -= 1
	if eval_count <= 0:
		$EvalTimer.stop()
		
	if state == "evaluating_timer":
		if eval_count <= 5:
			$Numbers.visible = true
			$Numbers.frame = eval_count
			
	if state == "evaluating_timer" and eval_count <= 0:
		$Numbers.visible = false
		state = "dialog"
		$Camera2D.do_zoom_in()
		$"Next Level/NopeDialog".popup_centered()


func _on_Button2_pressed():
	get_tree().change_scene("res://scenes/StartScreen.tscn")
