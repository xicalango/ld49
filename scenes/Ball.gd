extends RigidBody2D

export var lifetime = 2
export var damage = 1

export (PackedScene) var ball_properties_scene

export (PackedScene) var destroy_particles_scene

var time_alive = 0

var ball_properties: Node

func _ready():
	if ball_properties_scene != null:
		ball_properties = ball_properties_scene.instance()
		add_child(ball_properties)
	$CollisionShape2D.shape

func _process(delta):
	time_alive += delta
	
	if time_alive > lifetime:
		_kill()
	
func _on_Area2D_area_entered(area: Area2D):
	var node = area.get_parent()
	if ball_properties != null and "on_hit" in ball_properties:
		ball_properties.on_hit(node)
	else:
		_on_hit(node)

func _on_hit(node: Node2D):
	node.on_hit(self)
	_kill()

func _kill():
	var destroy_partilces: Node = destroy_particles_scene.instance()
	destroy_partilces.transform = transform
	destroy_partilces.emitting = true
	get_parent().add_child(destroy_partilces)
	queue_free()
