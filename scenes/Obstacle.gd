extends RigidBody2D

export var hitpoints: int = 2

onready var hitpoints_left = hitpoints

var hitpoints_label: Label

func _ready():
	continuous_cd = CCD_MODE_CAST_RAY
	hitpoints_label = Label.new()
	hitpoints_label.text = str(hitpoints_left)
	add_child(hitpoints_label)

func on_hit(ball):
	hitpoints_left -= 1
	hitpoints_label.text = str(hitpoints_left)
	#hitpoints_label.update()
	if hitpoints_left <= 0:
		queue_free()
