extends Node

var levels = [
	preload("res://scenes/Tutorial1.tscn"),
	preload("res://scenes/Tutorial2.tscn"),
	preload("res://scenes/Level1.tscn"),
	preload("res://scenes/Level2.tscn"),
	preload("res://scenes/Level4.tscn"),
	preload("res://scenes/Level3.tscn"),
	preload("res://scenes/Level6.tscn"),
	preload("res://scenes/Level7.tscn"),
	preload("res://scenes/Level5.tscn"),
	preload("res://scenes/Level8.tscn"),
]

var passwords = []

var current_level = 0

var num_balls = 10

func _ready():
	for level in levels:
		var level_inst = level.instance()
		passwords.append(level_inst.password)
		level_inst.free()

