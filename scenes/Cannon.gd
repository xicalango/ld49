extends Node2D

signal started
signal stopped

onready var _debug = get_node("/root/Debug")
onready var _variables = get_node("/root/Variables")
onready var balls_node = $"../Balls"
onready var debug_label = $CannonParent/CountLabel

export (PackedScene) var ball_scene

export var min_power: float = 100
export var max_power: float = 1000

export var charge_speed: float = 1

var power: Vector2 = Vector2(10, 0)
var current_balls: int = 0

var started = false

var drag = false

var charge = false
var charger = 0.0


# Called when the node enters the scene tree for the first time.
func _ready():
	$CannonParent/CountLabel.visible = _debug.show_damage_labels
	$CannonParent/CountLabel.text = str(_variables.num_balls)
	$CannonParent/fire/AnimationPlayer.playback_active = false

func reset():
	drag = false
	started = false
	charge = false
	charger = 0.0
	$wheel/FireButton.visible = true
	$CannonParent/CountLabel.text = str(_variables.num_balls)
	$Timer.stop()

func disable():
	$wheel/FireButton.visible = false

func start():
	charge = false
	current_balls = _variables.num_balls
	started = true
	$Timer.start()
	$wheel/FireButton.visible = false
	emit_signal("started")

func set_power(new_power_weight: float):
	new_power_weight = clamp(new_power_weight, 0, 1)
	var actual_power = lerp(min_power, max_power, new_power_weight)
	$CannonParent/fire/AnimationPlayer.current_animation = "FireStrength"
	$CannonParent/fire/AnimationPlayer.seek(new_power_weight, true)
	#$CannonParent/fire/AnimationPlayer.stop()
	power.x = actual_power
	
func set_angle(new_angle: float):
	new_angle = clamp(new_angle, deg2rad(-90), deg2rad(20))
	power.y = new_angle
	$CannonParent.rotation = new_angle

func _process(delta):
	if charge:
		charger += delta * charge_speed
		set_power(charger)
		if charger >= 1:
			start()

func _on_Timer_timeout():
	if current_balls == 0:
		if started:
			started = false
			$Timer.stop()
			emit_signal("stopped")
		return
	current_balls -= 1
	var new_ball: RigidBody2D = ball_scene.instance()

	new_ball.transform = $CannonParent/Nozzle.global_transform
	var cart_power = polar2cartesian(power.x, power.y)
	new_ball.apply_impulse(Vector2(), cart_power)
	
	balls_node.add_child(new_ball)
	
	debug_label.text = str(current_balls)

func _input(event):
	if event is InputEventMouseMotion and drag:
		set_angle(event.position.angle_to_point(position))
		#set_power(((position.distance_to(event.position)-100) /200))
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and not event.pressed:
			drag = false

func _on_Area2D_input_event(viewport, event, shape_idx):
	if started == true:
		return
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			drag = true

func _on_FireButton_button_up():
	start()

func _on_FireButton_button_down():
	charger = 0
	charge = true
